# A Samurai VM
To be used for playing around with Samurai.

Don't use this VM anywhere near a public facing network. It's ridiculously insecure.

### Requirements
- Vagrant (duh)
- Drush (`composer global require drush/drush:~8`)
- Ansible (`brew install ansible`)

### Install

Set up the Drupal docroot:
```sh
git clone git@github.com:jkswoods/samurai.git www/profiles/samurai
(cd www && drush make profiles/samurai/drupal-org-core.make --yes)
(cd www && drush make profiles/samurai/drupal-org.make --no-core --yes)
```

Set up drush aliases
```sh
mkdir drush
# Now stick your aliases in there.
```

Set up the vagrant machine:
```sh
ansible-galaxy install angstwad.docker_ubuntu
vagrant up --provision
vagrant reload # Docker doesn't like running on the first boot.
sudo echo "192.168.66.66 samurai" >> /etc/hosts
```

Visit http://samurai, run through the installer. Database name is `samurai`, user is `root` and the password is blank.

Mailcatcher is installed, any mail sent by PHP will be available at http://samurai:1080

### Todo

- Drush alias stuffs.
