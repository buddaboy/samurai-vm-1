# -*- mode: ruby -*-
# vi: set ft=ruby :

vm_name = "Samurai"
ip_address = "192.168.66.66"

# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  config.vm.define :samurai do |samurai|
    # Download the box.
    samurai.vm.box = "trusty"
    samurai.vm.box_url = "https://cloud-images.ubuntu.com/vagrant/trusty/current/trusty-server-cloudimg-amd64-vagrant-disk1.box"

    # Set a hostname.
    samurai.vm.hostname = vm_name.downcase

    # Shared folders.
    samurai.vm.synced_folder ".", "/vagrant", nfs: true
    samurai.vm.synced_folder "www/", "/var/www", nfs: true
    samurai.vm.synced_folder "drush/", "/etc/drush", nfs: true

    # Virtualbox specific config.
    samurai.vm.provider :virtualbox do |vb, override|
      # Give VM 1/4 system memory & access to all cpu cores on the host
      # https://stefanwrobel.com/how-to-make-vagrant-performance-not-suck
      host = RbConfig::CONFIG['host_os']
      if host =~ /darwin/
        cpus = `sysctl -n hw.ncpu`.to_i
        # sysctl returns Bytes and we need to convert to MB
        mem = `sysctl -n hw.memsize`.to_i / 1024 / 1024 / 4
      elsif host =~ /linux/
        cpus = `nproc`.to_i
        # meminfo shows KB and we need to convert to MB
        mem = `grep 'MemTotal' /proc/meminfo | sed -e 's/MemTotal://' -e 's/ kB//'`.to_i / 1024 / 4
      else # sorry Windows folks, I can't help you
        cpus = 1
        mem = 1024
      end
      vb.customize ["modifyvm", :id, "--memory", mem]
      vb.customize ["modifyvm", :id, "--cpus", cpus]

      # Configure network.
      vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
      override.vm.network :private_network, ip: ip_address

      # Give it a name, so you feel bad when you destroy it.
      vb.name = vm_name
    end

    # Ansible provisioning.
    samurai.vm.provision :ansible do |ansible|
      ansible.playbook = "provision.yml"
      ansible.host_key_checking = "False"
      ansible.extra_vars = { user: "vagrant" }
      ansible.extra_vars = { machine_name: vm_name.downcase }
      # ansible.skip_tags = "skipvg"
      # ansible.verbose = "vvvv"
    end
  end

end
